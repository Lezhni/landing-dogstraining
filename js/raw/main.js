$ = jQuery;

$(document).on('ready', ready);
$(document).on('scroll', scroll);
$(window).on('resize', resize);

$('.popup').click(function() {

    var target = $(this).attr('href');
    $.magnificPopup.open({
        items: {
            src: target,
            type: 'inline'
        }
    });

    return false;
});

$('.header-menu a, .header-callback').click(function() {

    var block = $(this).attr('href');
    if ($(window).width() > 1000) {
        var header = 125;
    } else {
        var header = 167;
    }
    var path = $(block).offset().top - header;
    $('body, html').animate({
        scrollTop: path
    }, 800);

    return false;
});

$('form').submit(function() {

    var form = $(this);
    var formData = form.serialize();

    form.find('input[type=submit]').attr('disabled', 'disabled').val('Отправляем...');

    if (formData) {
        $.post('send.php', formData, function(data, textStatus, xhr) {
            if (data == 'sended') {
                $.magnificPopup.open({
                    items: {
                        src: '#thanks',
                        type: 'inline'
                    }
                });
            } else {
                console.log(data);
            }
        });
    }

    form.find('input[type=submit]').removeAttr('disabled').val('Заказать звонок');

    return false;
})

$('.video-slide img').click(function() {
    var videoID = $(this).attr('data-video');
    var videoIframe = '<iframe width="680" height="380" src="https://www.youtube.com/embed/' + videoID + '?autoplay=1" frameborder="0" allowfullscreen></iframe>';
    $(this).replaceWith(videoIframe);
});

function ready() {

    $('.video-slider').slick({
        dots: true,
        prevArrow: '<span class="video-slider-prev"></span>',
        nextArrow: '<span class="video-slider-next"></span>'
    });
}

// #scrollspy
$('.animate').on('scrollSpy:enter', function() {

   if (!$(this).hasClass('visible')) {
       $(this).addClass('visible');
   }
});
$('.animate').scrollSpy();


function scroll() {}

function resize() {}
