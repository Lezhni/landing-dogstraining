<?php

if ($_POST) {
    require 'phpmailer/PHPMailerAutoload.php';

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';
    $mail->setFrom('admin@canis.pro', 'Кинологический центр');
    $mail->addReplyTo('canispro@yandex.ru', 'Кинологический центр');
    $mail->addAddress('canispro@yandex.ru');
    $mail->isHTML(true);

    $mail->Subject = 'Заявка с лендинга';
    $mail->Body = "<b>Номер телефона:</b> {$_POST['phone']}";

    if ($_POST['name']) $mail->Body .= "<br><br><b>Имя:</b> {$_POST['name']}";
    if ($_POST['dog_breed']) $mail->Body .= "<br><br><b>Порода собаки:</b> {$_POST['dog_breed']}";
    if ($_POST['dog_age']) $mail->Body .= "<br><br><b>Возраст собаки:</b> {$_POST['dog_age']}";
    if ($_POST['time'] != 0) $mail->Body .= "<br><br><b>Удобное время занятий:</b> {$_POST['time']}";
    if ($_POST['subway']) $mail->Body .= "<br><br><b>Ближайшая станция метро:</b> {$_POST['subway']}";
    if ($_POST['highway']) $mail->Body .= "<br><br><b>Шоссе и растояние в км:</b> {$_POST['highway']}";
    if ($_POST['message']) $mail->Body .= "<br><br><b>Дополнительные сведения:</b> {$_POST['message']}";

    if($mail->send()) {
        echo 'sended';
    } else {
        echo $mail->ErrorInfo;
    }
}

die();
